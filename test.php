<?php

    // 定義
    const PLUS     = '1';
    const MINUS    = '2';
    const MULTI    = '3';
    const DIVISION = '4';

    const OPERATOR = [
        '＋' => PLUS,
        '+'  => PLUS,
        'ー' => MINUS,
        '-'  => MINUS,
        '＊' => MULTI,
        '*'  => MULTI,
        '×'  => MULTI,
        '÷'  => DIVISION,
        '／' => DIVISION,
        '/'  => DIVISION,
    ];

    /**
     * 渡された演算子と２つの数字をもとに計算する
     * 渡された演算子が判断できない場合は $value1 を返却する
     * @param $operator 演算子
     * @param $value1   数字１
     * @param $value2   数字２
     */
    function calculate($operatorArg, $value1, $value2) {
        
        // 計算結果にデフォルトで $value1 を設定する
        $result = $value1;

        // 演算子を1~4の数字に変換する
        $operator = OPERATOR[$operatorArg];

        // 1~4に変換された演算子を判断して $value1 と $value2 を計算する
        switch ($operator) {
            case PLUS:
                $result = $value1 + $value2;
                break; 
            case MINUS:
                $result = $value1 - $value2;
                break;
            case MULTI:
                $result = $value1 * $value2;
                break;
            case DIVISION:
                $result = $value1 / $value2;
                break;
            default:
                break;
        }

        return $result;
    }

    // 入力
    $inputValues = [1, '+', 2, '-', 3, '+', 2, '*', 5];

    // 入力で渡された値を分解して保存するための一時変数
    $ope = null; // 演算子
    $res = null; // 結果

    // 計算
    foreach ($inputValues as $val) {

        // ループで回している値が演算子か数字なのか判断する
        if ('string' === gettype($val)) {
            // ループしている値が演算子（文字列）だったら
            $ope = $val;
        }

        // 計算するしないか判断する
        if (is_null($res)) {
            // 最初の一個の数字は計算せずに $res に入れる
            $res = $val;
        } elseif('integer' === gettype($val)) {
            // 2こめ以降の値が数字だった場合、計算する。演算子だったら計算せずにスキップする
            $res = calculate($ope, $res, $val);
            $message = '計算した';
        } else {
            $message = '計算しない';
        }

        echo "----------\n";
        echo "過程：" .$message . "\n";
        echo "ope：" . $ope . "\n";
        echo "val：" . $val . "\n";
        echo "res：" . $res . "\n";
    }

    echo "----------\n";
    echo "計算結果\n";
    echo $res;

